class CreateFilms < ActiveRecord::Migration
  def change
    create_table :films do |t|
      t.string :title
      t.text :text

      t.timestamps null: false
    end
  end
end
