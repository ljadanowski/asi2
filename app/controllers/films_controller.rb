class FilmsController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "admin", except: [:index, :show]

def index
  if params[:search]
    @films = Film.search(params[:search]).order("created_at DESC")
  else
    @films = Film.all.order('created_at DESC')
  end
end


  def search
    @films = Film.find(params[:id])
  end



  def show
    @film = Film.find(params[:id])
  end

  def new
    @film = Film.new
  end

  def edit
    @film = Film.find(params[:id])
  end

  def create
    @film = Film.new(film_params)
 
    if @film.save
      redirect_to @film
    else
      render 'new'
    end
  end
  def update
    @film = Film.find(params[:id])
 
    if @film.update(film_params)
      redirect_to @film
    else
      render 'edit'
    end
  end

  def destroy
    @film = Film.find(params[:id])
    @film.destroy
   
    redirect_to films_path
  end
 
   private
     def film_params
       params.require(:film).permit(:title, :text)
     end
end
