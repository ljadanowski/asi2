class Film < ActiveRecord::Base
 has_many :comments, dependent: :destroy
 validates :title, presence: true,
                    length: { minimum: 3 }


def self.search(query)
  where("title like ?", "%#{query}%") 
end


end
